#!/bin/bash
set -e
rm -Rf build bin
mkdir build 
cd build
cmake -DCMAKE_BUILD_TYPE="Release" ..
make 


